import React from "react";
import Slider from "react-slick";

class HelpSlides extends React.Component {
  render() {
    var settings = {
      dots: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false
    };
    return (
      <Slider {...settings}>
        <div>
          <figure>
            <img src="/images/gesture-1.png" alt='' />
            <figcaption>
              <p>Hold the phone in front of you or at a comfortable position.</p>
            </figcaption>
          </figure>
        </div>
        <div>
          <figure>
            <img src="/images/gesture-2.png" alt='' />
            <figcaption>
            <p>
              PRESS and HOLD finger on bottle cap. This is the starting position.
            </p>
            </figcaption>
          </figure>
        </div>
        <div>
          <figure>
            <img src="/images/gesture-3.png" alt='' />
            <figcaption>
            <p>
              While PRESSED, TILT phone towards you simulating opening motion.
            </p>
            </figcaption>
          </figure>
        </div>
        <div>
          <figure>
            <img src="/images/gesture-4.png" alt='' />
            <figcaption>
            <p>
              TILT phone until indicator signals  that it is ready to open, then RELEASE hold!
            </p>
            </figcaption>
          </figure>
        </div>
      </Slider>
    );
  }
}

export default HelpSlides
