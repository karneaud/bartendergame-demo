import React from "react";
import Slider from "react-slick";
import { connect } from 'react-redux';
import { pickBottle } from '../store/bottle/actions';

class PickerSlides extends React.Component {
  constructor(props) {
      super(props);

      this.state = {
        bottles: []
      }
  }

  componentDidMount() {
    fetch(process.env.PUBLIC_URL.concat('/data/bottles.json'))
    .then(function(response) {
      return response.json();
    })
    .then(json => {
      this.setState({bottles: json })
    });
  }

  componentWillUpdate(props) {
    if (props.bottle) this.props.enableButton()
  }

  render() {
    let settings = {
      dots: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      adaptiveHeight: true,
      focusOnSelect: true,
      centerMode: true,
      centerPadding: "25px"
    }, self = this, isSelected = self.props.bottle && self.props.bottle.id? self.props.bottle.id : 0;
    return (
      <Slider {...settings}>
        {this.state.bottles.map((bottle, i) =>
           <div key={bottle.id}>
             <figure className={`mx-auto ${ isSelected === bottle.id? 'selected' : '' }`}>
                 <img onClick={ (e) => { self.props.pickBottle(bottle) } } className="mx-auto bottle" src={ "/images/bottles/".concat(bottle.id,".png") } alt={bottle.name} />
                 <figcaption className="text-center">
                   <p>{bottle.name}</p>
                 </figcaption>
             </figure>
            </div>
           )}
      </Slider>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  bottle: state
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  pickBottle: (bottle) => dispatch(pickBottle(bottle))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PickerSlides)
