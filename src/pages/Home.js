import React from "react";
import logo from '../images/intro.svg';
import FacebookLoginWithButton from 'react-facebook-login';
import { Link } from 'react-router-dom';

const responseFacebook = (response) => {
  console.log(response);
}

class Home extends React.Component {
  render () {
    return (
      <div id="intro" className="panel">
          <section className="row flexbox">
            <header>
              <hgroup className="py-12">
                <h3 className="heading-size-2">The</h3>
                <h1 className="heading-size-1">Opener Bottle Bartender</h1>
                <h3 className="heading-size-2">Game</h3>
              </hgroup>
            </header>
            <article className="flexbox row p-12">
              <figure>
                <img src={logo} alt='Opener Bottle Bartender'/>
                </figure>
              <div>
                <p>
                <FacebookLoginWithButton
                    appId="477427772716975"
                    fields="name,email,picture"
                    callback={responseFacebook}
                    icon="fa-facebook" />
                    </p>
                    <p><strong>OR</strong></p>
                      <p>
                        <Link to="/how-to" target="_self" className="button">Continue</Link>
                      </p>
              </div>
            </article>
          </section>
        </div>
    );
  }
};

export default Home;
