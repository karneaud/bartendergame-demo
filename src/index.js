import React from 'react';
import ReactDOM from 'react-dom';
import './sass/index.scss';
import Application from './pages/App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { store } from './store/bottle/';

ReactDOM.render(<Provider store={ store }><Application /></Provider>, document.getElementById('root'));
registerServiceWorker();
