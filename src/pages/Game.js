import React from "react";
import { connect } from "react-redux";
import GameObjects from '../components/GameObjects';
import Modal from 'react-responsive-modal';

import '../sass/game.scss';

class GamePlay extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        bottlesOpened: 0,
        time: 0,
        tries: 3,
        open: true
      }

      this.onCloseModal.bind(this)
  }

  onCloseModal = () => {
    this.setState({ open: false });
  };

  render() {

    return (
      <div id="game" className="panel" >
          <section className="row flexbox">
            <header>
              <hgroup>
                <h2 id="tries" className="tries">Tries: <span>{ this.state.tries }</span></h2>
                <h3 className="bottles">Opened:<span id="score" className="opened">{ this.state.bottlesOpened }</span></h3>
              </hgroup>
            </header>
            <article className="row flexbox">
              <GameObjects drinkType={ this.props.bottle.id } />
                <Modal closeOnOverlayClick={ false } open={this.state.open} onClose={this.onCloseModal} center>
                  <h1>Get Ready</h1><p>Click to begin!</p><p><a href="javascript:void(0);" id="start-button" onClick={this.onCloseModal} className="button">START</a></p>
                </Modal>
            </article>
            </section>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => state

export default connect(
  mapStateToProps
)(GamePlay)
