import React from "react";
import { Helmet } from "react-helmet";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './Home';
import Help from './Help';
import StartPage from './Start';
import GamePlay from './Game';

import '../sass/home.scss';

class Application extends React.Component {
  render () {
    return (
        <main>
          <Helmet>
            <meta charset="utf-8"/>
            <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1,maximum-scale=1,user-scalable=0"/>
            <meta property="og:title" content="The Bartender Opener Bottle Game"/>
            <meta property="og:type" content="game"/>
            <meta property="og:url" content="http://www.kendallarneaud.me/mobile/bartendergame/"/>
            <meta property="og:image" content="http://www.kendallarneaud.me/mobile/bartendergame/images/intro.svg?v=0"/>
            <meta property="og:site_name" content="The Opener Bottle Bartender Game"/>
            <meta property="og:country-name" content="Trinidad"/>
            <meta property="og:description" name="description" content="The HTML5 mobile web-based game to test your bartending skills! How many bottles can you open?"/>
            <meta property="fb:app_id" content="685150489" />
            <meta name="mobile-web-app-capable" content="yes"/>
            <link rel="shortcut icon" href="favicon.ico"/>
            <link rel="icon" sizes="192x192" href="bottle-open.png"/>
            <meta name='apple-touch-fullscreen' content='yes'/>
            <meta name='apple-mobile-web-app-capable' content='yes'/>
            <link href='images/intro.svg' rel='apple-touch-startup-image'/>
            <meta name='apple-mobile-web-app-status-bar-style' content='black'/>
            <link rel="apple-touch-icon" sizes="128x128" href="bottle-open.png"/>
            <meta name="HandheldFriendly" content="true"/>
            <title>The Opener Bottle Bartender Game</title>
            <meta name="AUTHOR" content="Kendall Arneaud"/>
            <meta name="keywords" content="beer,alcohol,drinks,bottle,bartender,bottle cap,opener,booze" />
            <link href='http://www.kendallarneaud.me/mobile/bartendergame/images/intro.svg?v=1' rel='image_src' />
          </Helmet>
              <Router>
                <Switch>
                  <Route path="/how-to" component={ Help } />
                  <Route path="/start" component={ StartPage } />
                  <Route path="/game" component={ GamePlay } />
                  <Route exact path="/" component={ Home } />
                </Switch>
              </Router>
        </main>
    );
  }
};

export default Application;
