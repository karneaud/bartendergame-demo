import React from "react";

class GameObjects extends React.Component {

   render() {

    return (
      <div className="container">
        <div className="beer first" style={{ backgroundImage: 'url(/images/bottles/'.concat(this.props.drinkType, '.png)') }}>
          <div className="cap"></div>
        </div>
        <div className="beer second" style={{ backgroundImage: 'url(/images/bottles/'.concat(this.props.drinkType, '.png)') }}>
          <div className="cap"></div>
        </div>
        <div className="beer last" style={{ backgroundImage: 'url(/images/bottles/'.concat(this.props.drinkType, '.png)') }}>
          <div className="cap"></div>
        </div>
      </div>
    )
  }
}

export default GameObjects
