import React from "react";
import PickerSlides from '../components/Picker';
import { Link } from "react-router-dom";

import '../sass/start.scss';

class StartPage extends React.Component {
  constructor(props) {
      super(props);

      this.state = {
        enable: false
      }

      this.enableButton = this.enableButton.bind(this);
  }

  enableButton() {
    this.setState({ enable: true })
  }

  render() {
    return (
      <div id="start" className="panel" >
          <section className="row flexbox">
            <header>
              <hgroup>
                <h1>Pick Beer Bottle</h1>
                <p>
                  Choose your drink...Swipe to view, click to select!
                </p>
              </hgroup>
            </header>
            <article className="row flexbox">
                <div><PickerSlides enableButton={ this.enableButton } /></div>
                <p className="my-12 py-12">
                  <Link to="/game" target="_self" disabled={!this.state.enable} className={`button ${ this.state.enable? 'enabled' : 'disabled' }`}>Start Game</Link>
                </p>
            </article>
            </section>
      </div>
    );
  }
}

export default StartPage
