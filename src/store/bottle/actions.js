export const pickBottle = (bottle) => ({
  type: "PICK_BOTTLE",
  bottle
})
