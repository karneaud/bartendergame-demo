import React from "react";
import HelpSlides from '../components/Slide';
import { Link } from "react-router-dom";

import '../sass/help.scss';

class Help extends React.Component {
  render()  {
    return (
      <div id="instructions" className="panel">
          <section className="row flexbox">
            <header>
              <hgroup>
                <h1>What to do?</h1>
                <p>Slide to see...</p>
              </hgroup>
            </header>
            <article className="flexbox row p-12">
              <div>
                <HelpSlides/>
                </div>
                <p>
                  <Link to="/start" target="_self" className="button">Skip</Link>
                </p>
            </article>
          </section>
        </div>
    );
  }
}

export default Help
